#include <windows.h>
#include <wincodec.h>
#include <dwrite_3.h>
#include <windowsx.h>
#include <iostream>
#include <iomanip>
#include <numbers>
#include <cmath>
#include <d2d1.h>
#include <iostream>
#include <stdlib.h>
#include <utility>
#include "mug.h"
#include "kettle.h"

#pragma comment(lib, "d2d1")

#include "basewin.h"
using D2D1::BitmapProperties;
using D2D1::PixelFormat;
using D2D1::Point2F;
using D2D1::Point2U;
using D2D1::RectF;
using D2D1::RectU;
using D2D1::SizeU;
using D2D1::Matrix3x2F;
using std::sin;
using std::array;

class MainWindow : public BaseWindow<MainWindow> {

	enum move {
		none,
		kettle,
		mug,
		tea
	};

	ID2D1Factory* pFactory;
	ID2D1HwndRenderTarget* pRenderTarget;

	ID2D1PathGeometry* lamp_Geometry;
	ID2D1PathGeometry* switch_Geometry;
	ID2D1PathGeometry* painting_Geometry;

	ID2D1SolidColorBrush* pBrush;
	ID2D1SolidColorBrush* pNoseBrush;
	ID2D1SolidColorBrush* pGBrush;
	ID2D1SolidColorBrush* pLLLPosterBrush;

	D2D1_COLOR_F const background_color =
	{ 0.90f, 0.55f,  0.40f, 1.0f };

	IDWriteFactory* write_factory = nullptr;
	IDWriteTextFormat* text_format = nullptr;
	IDWriteTextFormat* poster_format = nullptr;

	int inst = 0;
	WCHAR const NAPIS[31] = L"Do you know how to make a tea?";
	WCHAR const POSTER[19] = L"Live, laugh & love";
	WCHAR const INST1[21] = L"It's a bit dark here";
	WCHAR const INST2[18] = L"Where is the mug";

	float mouse_x = 0;
	float mouse_y = 0;
	bool clicked = false;

	ID2D1Bitmap* kitchenBitmap = nullptr;
	ID2D1Bitmap* cupboardBitmap = nullptr;
	ID2D1Bitmap* fridgeBitmap = nullptr;
	ID2D1Bitmap* dayBitmap = nullptr;
	ID2D1Bitmap* lightBitmap = nullptr;
	ID2D1Bitmap* gasBitmap = nullptr;

	IWICImagingFactory* m_pIWICFactory;

	ID2D1RadialGradientBrush* pBodyBrush = nullptr;
	ID2D1GradientStopCollection* rad_stops = nullptr;
	D2D1_GRADIENT_STOP rad_stops_data[3];

	D2D1::Matrix3x2F transformation;

	Kettle kettle_object;
	Mug mug_object;

	bool light_switch = false;

	bool interactive = false;

	int daytime = 0;
	int day_x = 0;
	int day_y = 0;

	float cen_x = 0;
	float cen_y = 0;


	//float kettle_x = 300;
	//float kettle_y = -35;

	float paint_x = 520;
	float paint_y = -55;

	bool pouring = false;

	bool movin_painting = false;

	bool cupboard_open = false;
	bool low_cupboard_open = false;
	bool fridge_open = false;

	bool gas_range_1 = false;
	bool gas_range_2 = false;
	bool gas_range_3 = false;
	bool gas_range_4 = false;
	bool oven_on = false;

	move moved_object = none;
	float angle = 25;

	void    CalculateLayout();
	HRESULT MainWindow::LoadBitmapFromFile(
		ID2D1RenderTarget* pRenderTarget,
		IWICImagingFactory* pIWICFactory,
		ID2D1Bitmap** pBitMap,
		PCWSTR uri,
		UINT destinationWidth,
		UINT destinationHeight
	);
	HRESULT CreateGraphicsResources();
	void    DiscardGraphicsResources();
	void    OnPaint();
	void    Resize();

public:

	MainWindow() : pFactory(nullptr), pRenderTarget(nullptr), pBrush(nullptr)
	{
	}

	PCWSTR  ClassName() const { return L"Project2D Window Class"; }
	LRESULT HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam);
};