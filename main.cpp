#include "MainWindow.h"
#include "paint.h"

UINT const bmp_width = 2;
UINT const bmp_height = 2;


template <class T> void SafeRelease(T** ppT) {
	if (*ppT)
	{
		(*ppT)->Release();
		*ppT = nullptr;
	}
}

void MainWindow::CalculateLayout() {
	if (pRenderTarget != nullptr) {
		D2D1_SIZE_F size = pRenderTarget->GetSize();
		cen_x = size.width / 2;
		cen_y = size.height / 2;
	}
}

HRESULT MainWindow::LoadBitmapFromFile(
	ID2D1RenderTarget* pRenderTarget,
	IWICImagingFactory* pIWICFactory,
	ID2D1Bitmap** pBitMap,
	PCWSTR uri,
	UINT destinationWidth,
	UINT destinationHeight
)
{
	IWICBitmapDecoder* pDecoder = nullptr;
	IWICBitmapFrameDecode* pSource = nullptr;
	IWICStream* pStream = nullptr;
	IWICFormatConverter* pConverter = nullptr;
	IWICBitmapScaler* pScaler = nullptr;

	HRESULT hr = pIWICFactory->CreateDecoderFromFilename(
		uri,
		nullptr,
		GENERIC_READ,
		WICDecodeMetadataCacheOnLoad,
		&pDecoder
	);

	hr = pDecoder->GetFrame(0, &pSource);

	hr = pIWICFactory->CreateFormatConverter(&pConverter);

	hr = pConverter->Initialize(
		pSource,
		GUID_WICPixelFormat32bppPBGRA,
		WICBitmapDitherTypeNone,
		nullptr,
		0.f,
		WICBitmapPaletteTypeMedianCut
	);
	if (SUCCEEDED(hr))
	{

		// Create a Direct2D bitmap from the WIC bitmap.
		hr = pRenderTarget->CreateBitmapFromWicBitmap(
			pConverter,
			nullptr,
			pBitMap
		);

		SafeRelease(&pDecoder);
		SafeRelease(&pSource);
		SafeRelease(&pStream);
		SafeRelease(&pConverter);
		SafeRelease(&pScaler);

		return hr;
	}
}

HRESULT MainWindow::CreateGraphicsResources() {
	HRESULT hr = S_OK;
	if (pRenderTarget == nullptr) {
		RECT rc;
		GetClientRect(m_hwnd, &rc);

		D2D1_SIZE_U size = D2D1::SizeU(rc.right, rc.bottom);

		hr = pFactory->CreateHwndRenderTarget(
			D2D1::RenderTargetProperties(),
			D2D1::HwndRenderTargetProperties(m_hwnd, size),
			&pRenderTarget);

		if (FAILED(hr))
		{
			DiscardGraphicsResources();
			return -1;
		}

		hr = CoInitializeEx(nullptr, COINIT_APARTMENTTHREADED);

		hr = CoCreateInstance(
			CLSID_WICImagingFactory,
			nullptr,
			CLSCTX_INPROC_SERVER,
			__uuidof(IWICImagingFactory),
			reinterpret_cast<LPVOID*>(&m_pIWICFactory)
		);

		if (SUCCEEDED(hr)) {
			hr = pRenderTarget->CreateSolidColorBrush(D2D1::ColorF(0.0f, 0.0f, 0), &pBrush);
			hr = pRenderTarget->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::DimGray), &pNoseBrush);
			hr = pRenderTarget->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::Red), &pGBrush);
			hr = pRenderTarget->CreateSolidColorBrush(D2D1::ColorF(0xDEB887), &pLLLPosterBrush);


			write_factory->CreateTextFormat(
				L"Courier New",
				nullptr,
				DWRITE_FONT_WEIGHT_BOLD,
				DWRITE_FONT_STYLE_NORMAL,
				DWRITE_FONT_STRETCH_NORMAL,
				20.0f,
				L"en-us",
				&text_format
			);

			write_factory->CreateTextFormat(
				L"Gabriola",
				nullptr,
				DWRITE_FONT_WEIGHT_BOLD,
				DWRITE_FONT_STYLE_ITALIC,
				DWRITE_FONT_STRETCH_NORMAL,
				16.0f,
				L"en-us",
				&poster_format
			);


			hr = LoadBitmapFromFile(
				pRenderTarget,
				m_pIWICFactory, &dayBitmap,
				L"day_night.png", bmp_width, bmp_height
			);

			hr = LoadBitmapFromFile(
				pRenderTarget,
				m_pIWICFactory, &kitchenBitmap,
				L"kitchen.png", bmp_width, bmp_height
			);

			hr = LoadBitmapFromFile(
				pRenderTarget,
				m_pIWICFactory, &cupboardBitmap,
				L"cupboard.png", bmp_width, bmp_height
			);

			hr = LoadBitmapFromFile(
				pRenderTarget,
				m_pIWICFactory, &fridgeBitmap,
				L"fridge.png", bmp_width, bmp_height
			);

			hr = LoadBitmapFromFile(
				pRenderTarget,
				m_pIWICFactory, &mug_object.teaBitmap,
				L"tea.png", bmp_width, bmp_height
			);

			hr = LoadBitmapFromFile(
				pRenderTarget,
				m_pIWICFactory, &lightBitmap,
				L"lamp.png", bmp_width, bmp_height
			);

			hr = LoadBitmapFromFile(
				pRenderTarget,
				m_pIWICFactory, &gasBitmap,
				L"gas.png", bmp_width, bmp_height
			);

			hr = LoadBitmapFromFile(
				pRenderTarget,
				m_pIWICFactory, &kettle_object.kettleBitmap,
				L"kettle.png", bmp_width, bmp_height
			);

			hr = LoadBitmapFromFile(
				pRenderTarget,
				m_pIWICFactory, &mug_object.mugBitmap,
				L"mug.png", bmp_width, bmp_height
			);

			if (SUCCEEDED(hr)) {
				CalculateLayout();
			}
		}
		if (SUCCEEDED(hr)) {

			rad_stops_data[0] = { 0.2f, D2D1::ColorF(D2D1::ColorF::White) };
			rad_stops_data[1] =
			{ 0.4f, D2D1::ColorF(D2D1::ColorF::LightBlue), };
			rad_stops_data[2] =
			{ 1.0f, D2D1::ColorF(D2D1::ColorF::DarkCyan), };
			pRenderTarget->CreateGradientStopCollection(
				rad_stops_data, 3, &rad_stops);

			if (SUCCEEDED(hr)) {
				CalculateLayout();
			}
		}
	}
	return hr;
}

void MainWindow::DiscardGraphicsResources() {
	SafeRelease(&pRenderTarget);
	SafeRelease(&pBrush);
	SafeRelease(&pBodyBrush);
	SafeRelease(&pGBrush);
	SafeRelease(&pLLLPosterBrush);
	if (text_format) text_format->Release();
	if (poster_format) poster_format->Release();
	if (write_factory) write_factory->Release();
}

void MainWindow::OnPaint() {
	HRESULT hr = CreateGraphicsResources();
	if (SUCCEEDED(hr)) {

		hr = pFactory->CreatePathGeometry(&lamp_Geometry);

		if (SUCCEEDED(hr)) {
			ID2D1GeometrySink* pSink = NULL;

			hr = lamp_Geometry->Open(&pSink);
			if (SUCCEEDED(hr)) {

				float lamp_x = cen_x - 50;
				float lamp_y = cen_y - 270;

				pSink->SetFillMode(D2D1_FILL_MODE_WINDING);

				pSink->BeginFigure(D2D1::Point2F(lamp_x, lamp_y), D2D1_FIGURE_BEGIN_FILLED);
				pSink->AddBezier(D2D1::BezierSegment(
					D2D1::Point2F(lamp_x - 90, lamp_y + 30), D2D1::Point2F(lamp_x + 180, lamp_y + 30), D2D1::Point2F(lamp_x + 90, lamp_y)));
				pSink->AddQuadraticBezier(D2D1::QuadraticBezierSegment(
					D2D1::Point2F(lamp_x + 45, lamp_y - 45), D2D1::Point2F(lamp_x, lamp_y)));
				pSink->EndFigure(D2D1_FIGURE_END_OPEN);

			}
			hr = pSink->Close();

			SafeRelease(&pSink);
		}

		hr = pFactory->CreatePathGeometry(&switch_Geometry);

		if (SUCCEEDED(hr)) {
			ID2D1GeometrySink* pSink = NULL;

			hr = switch_Geometry->Open(&pSink);
			if (SUCCEEDED(hr)) {

				pSink->SetFillMode(D2D1_FILL_MODE_WINDING);

				pSink->BeginFigure(
					D2D1::Point2F(cen_x + 445, cen_y + 30),
					D2D1_FIGURE_BEGIN_FILLED
				);
				D2D1_POINT_2F points[4] = {
				   D2D1::Point2F(cen_x + 445, cen_y + 50),
				   D2D1::Point2F(cen_x + 455, cen_y + 53),
				   D2D1::Point2F(cen_x + 455, cen_y + 27),
				   D2D1::Point2F(cen_x + 445, cen_y + 30),
				};
				pSink->AddLines(points, ARRAYSIZE(points));
				pSink->EndFigure(D2D1_FIGURE_END_CLOSED);

			}
			hr = pSink->Close();
			SafeRelease(&pSink);
		}

		hr = pFactory->CreatePathGeometry(&painting_Geometry);

		if (SUCCEEDED(hr)) {
			ID2D1GeometrySink* pSink = NULL;

			hr = painting_Geometry->Open(&pSink);
			if (SUCCEEDED(hr)) {

				pSink->SetFillMode(D2D1_FILL_MODE_WINDING);

				if (clicked && (sqrt((mouse_x - cen_x - paint_x) * (mouse_x - cen_x - paint_x) + (mouse_y - cen_y - paint_y) * (mouse_y - cen_y - paint_y)) <= 55)
					) {
					pSink->BeginFigure(
						D2D1::Point2F(mouse_x - 35, mouse_y + 30),
						D2D1_FIGURE_BEGIN_FILLED
					);
					D2D1_POINT_2F points[4] = {
					 D2D1::Point2F(mouse_x - 35, mouse_y + 55),
					 D2D1::Point2F(mouse_x + 35, mouse_y + 50),
					 D2D1::Point2F(mouse_x + 35, mouse_y - 85),
					 D2D1::Point2F(mouse_x - 35, mouse_y - 65),
					};
					pSink->AddLines(points, ARRAYSIZE(points));
					/*      float left = max(mouse_x, cen_x - 430) - 30;
						  float right = min(left, cen_x + 240);*/
					paint_x = max(min(mouse_x - cen_x, 565), 495);
					paint_y = max(min(-(-mouse_y + cen_y), -35), -75);
					// kettle_x = max(min(-mouse_x + cen_x, 430), -270);

				}
				else {
					pSink->BeginFigure(
						D2D1::Point2F(cen_x + paint_x - 35, cen_y - 90),
						D2D1_FIGURE_BEGIN_FILLED
					);
					D2D1_POINT_2F points[4] = {
					   D2D1::Point2F(cen_x + paint_x - 35, cen_y + paint_y + 65),
					   D2D1::Point2F(cen_x + paint_x + 35, cen_y + paint_y + 60),
					   D2D1::Point2F(cen_x + paint_x + 35, cen_y + paint_y - 75),
					   D2D1::Point2F(cen_x + paint_x - 35, cen_y + paint_y - 55),
					};
					pSink->AddLines(points, ARRAYSIZE(points));
				}

				pSink->EndFigure(D2D1_FIGURE_END_CLOSED);

			}
			hr = pSink->Close();

			SafeRelease(&pSink);
		}

		pRenderTarget->BeginDraw();

		pRenderTarget->Clear(background_color);
		D2D1_SIZE_F rtSize = pRenderTarget->GetSize();

		draw_kitchen(pRenderTarget, kitchenBitmap, cen_x, cen_y);

		draw_window(pRenderTarget, dayBitmap, cen_x, cen_y, day_x, day_y);

		pRenderTarget->DrawText(
			NAPIS,
			sizeof(NAPIS) / sizeof(NAPIS[0]),
			text_format,
			RectF(
				static_cast<FLOAT>(cen_x - 400), static_cast<FLOAT>(cen_y - 340),
				static_cast<FLOAT>(cen_x + 200),
				static_cast<FLOAT>(cen_y - 240)
			),
			pBrush
		);

		pRenderTarget->FillEllipse(D2D1::Ellipse(D2D1::Point2F(cen_x + 230, cen_y - 6), 3, 3), pBrush);
		pRenderTarget->DrawLine(D2D1::Point2F(cen_x + 230, cen_y - 6),
			D2D1::Point2F(cen_x + 180, cen_y + 35), pBrush);
		pRenderTarget->DrawLine(D2D1::Point2F(cen_x + 230, cen_y - 6),
			D2D1::Point2F(cen_x + 280, cen_y + 35), pBrush);

		pRenderTarget->DrawRectangle(RectF(
			static_cast<FLOAT>(cen_x + 180), static_cast<FLOAT>(cen_y + 5),
			static_cast<FLOAT>(cen_x + 280),
			static_cast<FLOAT>(cen_y + 35)
		), pBrush, 3.0f);

		pRenderTarget->FillRectangle(RectF(
			static_cast<FLOAT>(cen_x + 180), static_cast<FLOAT>(cen_y + 5),
			static_cast<FLOAT>(cen_x + 280),
			static_cast<FLOAT>(cen_y + 35)
		), pLLLPosterBrush);

		if ((mouse_x > (cen_x + 180)) && (mouse_x < (cen_x + 280)) && (mouse_y > (cen_y + 5)) && (mouse_y < (cen_y + 35))) {
			interactive = true;
			if (clicked) {
				inst = (inst + 1) % 3;
			}
		}

		//draw text
		if (inst == 0) {
			pRenderTarget->DrawText(
				POSTER,
				sizeof(POSTER) / sizeof(POSTER[0]),
				poster_format,
				RectF(
					static_cast<FLOAT>(cen_x + 183), static_cast<FLOAT>(cen_y + 3),
					static_cast<FLOAT>(cen_x + 284),
					static_cast<FLOAT>(cen_y + 15)
				),
				pBrush
			);
		}
		else if (inst == 1) {
			pRenderTarget->DrawText(
				INST1,
				sizeof(INST1) / sizeof(INST1[0]),
				poster_format,
				RectF(
					static_cast<FLOAT>(cen_x + 181), static_cast<FLOAT>(cen_y + 3),
					static_cast<FLOAT>(cen_x + 284),
					static_cast<FLOAT>(cen_y + 15)
				),
				pBrush
			);
		}
		else {
			pRenderTarget->DrawText(
				INST2,
				sizeof(INST2) / sizeof(INST2[0]),
				poster_format,
				RectF(
					static_cast<FLOAT>(cen_x + 183), static_cast<FLOAT>(cen_y + 3),
					static_cast<FLOAT>(cen_x + 284),
					static_cast<FLOAT>(cen_y + 15)
				),
				pBrush
			);
		}

		if (rad_stops) {
			pRenderTarget->CreateRadialGradientBrush(
				D2D1::RadialGradientBrushProperties({ cen_x + 450, cen_y },
					D2D1::Point2F(0, 0), 250, 250),
				rad_stops, &pBodyBrush);
		}

		pRenderTarget->FillGeometry(switch_Geometry, pBrush);

		pRenderTarget->FillGeometry(painting_Geometry, pBodyBrush);
		pRenderTarget->DrawGeometry(painting_Geometry, pBrush, 2.f);

		// faucet
		if (sqrt((mouse_x - cen_x + 50) * (mouse_x - cen_x + 50) + (mouse_y - cen_y - 50) * (mouse_y - cen_y - 50)) <= 15) {
			interactive = true;
			if (clicked) {
				draw_water_1(pRenderTarget, dayBitmap, cen_x, cen_y);
			}
		}

		if (sqrt((mouse_x - cen_x - paint_x) * (mouse_x - cen_x - paint_x) + (mouse_y - cen_y - paint_y) * (mouse_y - cen_y - paint_y)) <= 55) {
			interactive = true;
		}

		if ((mouse_x < (cen_x - 250)) && (mouse_x > (cen_x - 270)) && (mouse_y < (cen_y - 20)) && (mouse_y > (cen_y - 50))) {
			interactive = true;
			if (clicked) {

				if (!mug_object.get_mug_open()) {
					mug_object.update_show_mug(!mug_object.if_show_mug());
					mug_object.update_show_tea(!mug_object.if_show_tea());
				}

				cupboard_open = !cupboard_open;
			}
		}

		if ((mouse_x < (cen_x + 45)) && (mouse_x > (cen_x + 25)) && (mouse_y < (cen_y + 170)) && (mouse_y > (cen_y + 150))) {
			interactive = true;
			if (clicked) {
				low_cupboard_open = !low_cupboard_open;
			}
		}


		draw_cupboards(pRenderTarget, cupboardBitmap, cen_x, cen_y, low_cupboard_open, cupboard_open);

		if ((mouse_x > (cen_x - 580)) && (mouse_x < (cen_x - 460)) && (mouse_y > (cen_y - 90)) && (mouse_y < (cen_y + 190))) {
			if (!fridge_open) {
				interactive = true;
			}
			if (clicked) {
				fridge_open = true;
			}
		}

		if ((mouse_x > (cen_x - 450)) && (mouse_x < (cen_x - 330)) && (mouse_y > (cen_y - 90)) && (mouse_y < (cen_y + 190))) {
			if (fridge_open) {
				interactive = true;
			}

			if (clicked) {
				fridge_open = false;
			}
		}

		if (fridge_open) {
			draw_fridge(pRenderTarget, fridgeBitmap, cen_x, cen_y);
		}

		// gas range
		if (sqrt((mouse_x - cen_x - 170) * (mouse_x - cen_x - 170) + (mouse_y - cen_y - 105) * (mouse_y - cen_y - 105)) <= 10) {
			interactive = true;
			if (clicked) {
				gas_range_1 = !gas_range_1;
			}
		}
		if (sqrt((mouse_x - cen_x - 145) * (mouse_x - cen_x - 145) + (mouse_y - cen_y - 105) * (mouse_y - cen_y - 105)) <= 10) {
			interactive = true;
			if (clicked) {
				gas_range_2 = !gas_range_2;
			}
		}
		if (sqrt((mouse_x - cen_x - 238) * (mouse_x - cen_x - 238) + (mouse_y - cen_y - 105) * (mouse_y - cen_y - 105)) <= 10) {
			interactive = true;
			if (clicked) {
				gas_range_3 = !gas_range_3;
			}
		}
		if (sqrt((mouse_x - cen_x - 257) * (mouse_x - cen_x - 257) + (mouse_y - cen_y - 105) * (mouse_y - cen_y - 105)) <= 10) {
			interactive = true;
			if (clicked) {
				gas_range_4 = !gas_range_4;
			}
		}
		if (sqrt((mouse_x - cen_x - 277) * (mouse_x - cen_x - 277) + (mouse_y - cen_y - 105) * (mouse_y - cen_y - 105)) <= 10) {
			interactive = true;
			if (clicked) {
				oven_on = !oven_on;
			}
		}

		update_range(pRenderTarget, gasBitmap, cen_x, cen_y, gas_range_1, gas_range_2, gas_range_3, gas_range_4);
		update_oven(pRenderTarget, lightBitmap, cen_x, cen_y, oven_on);

		if (sqrt((mouse_x - cen_x + kettle_object.get_mug().first) * (mouse_x - cen_x + kettle_object.get_mug().first) + (mouse_y - cen_y + kettle_object.get_mug().second) * (mouse_y - cen_y + kettle_object.get_mug().second)) <= 45) {
			interactive = true;
			if (moved_object == none && clicked) {
				moved_object = kettle;
			}
		}

		if (!clicked && moved_object == kettle) {
			moved_object = none;
			kettle_object.update_kettle(true, max(min(-mouse_x + cen_x, 300), -270));
		}

		if (moved_object != kettle && !(pouring && mug_object.get_mug_open())) {
			kettle_object.draw_stable(cen_x, cen_y, pRenderTarget);
		}

		if (moved_object == kettle && !(pouring && mug_object.get_mug_open())) {
			float left = max(mouse_x, cen_x - 300) - 30;
			float right = min(left, cen_x + 240);

			kettle_object.draw_moving(cen_x, cen_y, mouse_x, pRenderTarget);
		}

		if (sqrt((mouse_x - cen_x + mug_object.get_tea().first) * (mouse_x - cen_x + mug_object.get_tea().first) + (mouse_y - cen_y + mug_object.get_tea().second) * (mouse_y - cen_y + mug_object.get_tea().second)) <= 10) {
			if (cupboard_open || mug_object.get_tea().second == -66) {
				interactive = true;
			}
			if (moved_object == none && clicked) {
				moved_object = tea;
			}
		}

		if (moved_object != tea) {
			mug_object.draw_stable_tea(cen_x, cen_y, pRenderTarget);
		}

		if (moved_object == tea && mug_object.if_show_tea()) {
			mug_object.draw_moving_tea(cen_x, cen_y, mouse_y, pRenderTarget);
		}

		if (!clicked && moved_object == tea && mug_object.if_show_tea()) {
			moved_object = none;

			if (abs(mouse_y - cen_y + 71) <= abs(mouse_y - (cen_y + 66)) && cupboard_open) {
				mug_object.update_tea(false, 71);
			}
			else {
				mug_object.update_tea(false, -66);
			}
		}

		if (sqrt((mouse_x - cen_x + mug_object.get_mug().first) * (mouse_x - cen_x + mug_object.get_mug().first) + (mouse_y - cen_y + mug_object.get_mug().second) * (mouse_y - cen_y + mug_object.get_mug().second)) <= 20) {
			if (cupboard_open || mug_object.get_mug().second == -75) {
				interactive = true;
			}
			if (moved_object == none && clicked) {
				moved_object = mug;
			}
		}

		if (mug_object.get_mug().first - kettle_object.get_mug().first > 22 && mug_object.get_mug().first - kettle_object.get_mug().first < 35) {
			pouring = true;
		}
		else {
			pouring = false;
		}

		if (pouring && mug_object.get_mug_open()) {

			draw_water_2(pRenderTarget, dayBitmap, cen_x, cen_y);

			pRenderTarget->SetTransform(
				Matrix3x2F::Rotation(-angle, Point2F(cen_x - 60, cen_y + 10)) //(kettle_x + 220, kettle_y + 310))
			);

			kettle_object.draw_pouring(cen_x, cen_y, pRenderTarget);

			pRenderTarget->SetTransform(
				Matrix3x2F::Identity()
			);
		}

		if (!clicked && moved_object == mug && mug_object.if_show_mug()) {
			moved_object = none;

			mug_object.stop_moving(cen_x, cen_y, mouse_y, cupboard_open, pRenderTarget);
		}

		if (moved_object != mug) {
			mug_object.draw_stable(cen_x, cen_y, pRenderTarget);
		}

		if (moved_object == mug && (mug_object.if_show_mug())) {
			mug_object.draw_moving(cen_x, cen_y, mouse_y, pRenderTarget);
		}

		// switch the light
		if (sqrt((mouse_x - cen_x - 450) * (mouse_x - cen_x - 450) + (mouse_y - cen_y - 50) * (mouse_y - cen_y - 50)) <= 15) {
			interactive = true;
			if (clicked) {
				light_switch = !light_switch;
			}
		}

		if (light_switch) {
			draw_light(pRenderTarget, lightBitmap, cen_x, cen_y);
		}

		//lamp
		pRenderTarget->FillGeometry(lamp_Geometry, pBodyBrush);
		pRenderTarget->DrawGeometry(lamp_Geometry, pBrush, 2.f);

		pRenderTarget->DrawLine(
			D2D1::Point2F(cen_x - 5, cen_y - 293),
			D2D1::Point2F(cen_x - 5, cen_y - 350),
			pBrush,
			2.5f
		);

		if (interactive) {
			pRenderTarget->FillEllipse(D2D1::Ellipse(D2D1::Point2F(mouse_x, mouse_y), 5, 5), pGBrush);
		}
		else {
			pRenderTarget->FillEllipse(D2D1::Ellipse(D2D1::Point2F(mouse_x, mouse_y), 5, 5), pNoseBrush);
		}

		interactive = false;

		hr = pRenderTarget->EndDraw();
		if (FAILED(hr) || hr == D2DERR_RECREATE_TARGET)
		{
			DiscardGraphicsResources();
		}
		ValidateRect(m_hwnd, nullptr);

	}
}

void MainWindow::Resize() {
	if (pRenderTarget != nullptr) {
		RECT rc;
		GetClientRect(m_hwnd, &rc);

		D2D1_SIZE_U size = D2D1::SizeU(rc.right, rc.bottom);

		pRenderTarget->Resize(size);
		CalculateLayout();
		InvalidateRect(m_hwnd, nullptr, FALSE);
	}
}

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE, PWSTR, int nCmdShow)
{
	MainWindow win;

	if (!win.Create(L"Project2D", WS_OVERLAPPEDWINDOW)) {
		return 0;
	}


	ShowWindow(win.Window(), nCmdShow);

	// Run the message loop.

	MSG msg = { };
	while (GetMessage(&msg, nullptr, 0, 0)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return 0;
}

LRESULT MainWindow::HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam) {
	switch (uMsg)
	{
	case WM_CREATE:
		SetTimer(m_hwnd, 1000, 100, nullptr);
		if (FAILED(D2D1CreateFactory(
			D2D1_FACTORY_TYPE_SINGLE_THREADED, &pFactory))) {
			return -1;
		}

		kettle_object = Kettle();
		mug_object = Mug();

		if (FAILED(DWriteCreateFactory(
			DWRITE_FACTORY_TYPE_SHARED,
			__uuidof(IDWriteFactory),
			reinterpret_cast<IUnknown**>(&write_factory)))) {
			return -1;
		}
		return 0;

	case WM_DESTROY:
		DiscardGraphicsResources();
		SafeRelease(&pFactory);
		KillTimer(m_hwnd, 1000);
		PostQuitMessage(0);
		return 0;

	case WM_PAINT:
		OnPaint();
		return 0;

	case WM_LBUTTONDOWN:
		clicked = 1;
		return 0;

	case WM_LBUTTONUP:
		clicked = 0;
		return 0;

	case WM_MOUSEMOVE:
		mouse_x = GET_X_LPARAM(lParam);
		mouse_y = GET_Y_LPARAM(lParam);
		InvalidateRect(m_hwnd, nullptr, true);
		return 0;

	case WM_TIMER:
		daytime = (daytime + 1) % 60;
		if (daytime == 0) {
			day_x = (day_x + 1) % 6;
			if (day_x == 0) {
				day_y = (day_y + 1) % 3;
			}
		}

		InvalidateRect(m_hwnd, nullptr, true);
		return 0;


	case WM_SIZE:
		Resize();
		return 0;
	}

	return DefWindowProc(m_hwnd, uMsg, wParam, lParam);
}