#pragma once


void draw_kitchen(ID2D1HwndRenderTarget* pRenderTarget, ID2D1Bitmap* kitchenBitmap, float cen_x, float cen_y) {
	pRenderTarget->DrawBitmap(
		kitchenBitmap,
		RectF(cen_x - 620, cen_y - 350, cen_x + 620, cen_y + 350),
		1.0f,
		D2D1_BITMAP_INTERPOLATION_MODE_LINEAR);
}

void draw_window(ID2D1HwndRenderTarget* pRenderTarget, ID2D1Bitmap* dayBitmap, float cen_x, float cen_y, float day_x, float day_y) {
	pRenderTarget->DrawBitmap(
		dayBitmap,
		RectF(cen_x - 134., cen_y - 163., cen_x + 36., cen_y - 3.),
		1.0f,
		D2D1_BITMAP_INTERPOLATION_MODE_LINEAR,
		RectF(day_x * 735. + 20., day_y * 735. + 30., day_x * 730. + 730., day_y * 735. + 735.));
}

void draw_cupboards(ID2D1HwndRenderTarget* pRenderTarget, ID2D1Bitmap* cupboardBitmap, float cen_x, float cen_y, bool low_cupboard_open, bool cupboard_open) {
	if (low_cupboard_open) {
		pRenderTarget->DrawBitmap(
			cupboardBitmap,
			RectF(cen_x + 26, cen_y + 138, cen_x + 128, cen_y + 260),
			1.0f,
			D2D1_BITMAP_INTERPOLATION_MODE_LINEAR,
			RectF(5, 8, 530, 550));
	}

	if (cupboard_open) {
		pRenderTarget->DrawBitmap(
			cupboardBitmap,
			RectF(cen_x - 267, cen_y - 6, cen_x - 176, cen_y - 192),
			1.0f,
			D2D1_BITMAP_INTERPOLATION_MODE_LINEAR);
	}
}

void draw_fridge(ID2D1HwndRenderTarget* pRenderTarget, ID2D1Bitmap* fridgeBitmap, float cen_x, float cen_y) {
	pRenderTarget->DrawBitmap(
		fridgeBitmap,
		RectF(cen_x - 584, cen_y - 90.5, cen_x - 342.5, cen_y + 268),
		1.0f,
		D2D1_BITMAP_INTERPOLATION_MODE_LINEAR);
}

void update_range(ID2D1HwndRenderTarget* pRenderTarget, ID2D1Bitmap* gasBitmap, float cen_x, float cen_y, bool g1, bool g2, bool g3, bool g4) {

	if (g1) {
		pRenderTarget->DrawBitmap(
			gasBitmap,
			RectF(cen_x + 150, cen_y + 74, cen_x + 182, cen_y + 86),
			100.0f,
			D2D1_BITMAP_INTERPOLATION_MODE_LINEAR);
	}
	if (g2) {
		pRenderTarget->DrawBitmap(
			gasBitmap,
			RectF(cen_x + 142, cen_y + 64, cen_x + 174, cen_y + 76),
			100.0f,
			D2D1_BITMAP_INTERPOLATION_MODE_LINEAR);
	}
	if (g3) {
		pRenderTarget->DrawBitmap(
			gasBitmap,
			RectF(cen_x + 217, cen_y + 64, cen_x + 249, cen_y + 76),
			100.0f,
			D2D1_BITMAP_INTERPOLATION_MODE_LINEAR);
	}
	if (g4) {
		pRenderTarget->DrawBitmap(
			gasBitmap,
			RectF(cen_x + 232, cen_y + 74, cen_x + 264, cen_y + 86),
			100.0f,
			D2D1_BITMAP_INTERPOLATION_MODE_LINEAR);
	}
}

void update_oven(ID2D1HwndRenderTarget* pRenderTarget, ID2D1Bitmap* lightBitmap, float cen_x, float cen_y, bool oven) {
	if (oven) {
		pRenderTarget->DrawBitmap(
			lightBitmap,
			RectF(cen_x + 130, cen_y + 145, cen_x + 290, cen_y + 205),
			100.0f,
			D2D1_BITMAP_INTERPOLATION_MODE_LINEAR);
	}
}
void draw_water_1(ID2D1HwndRenderTarget* pRenderTarget, ID2D1Bitmap* dayBitmap, float cen_x, float cen_y) {
	pRenderTarget->DrawBitmap(
		dayBitmap,
		RectF(cen_x - 55, cen_y + 57, cen_x - 50, cen_y + 86),
		1.0f,
		D2D1_BITMAP_INTERPOLATION_MODE_LINEAR,
		RectF(20, 30, 38, 100));
}

void draw_water_2(ID2D1HwndRenderTarget* pRenderTarget, ID2D1Bitmap* dayBitmap, float cen_x, float cen_y) {
	pRenderTarget->DrawBitmap(
		dayBitmap,
		RectF(cen_x - 220, cen_y + 50, cen_x - 215, cen_y + 80),
		1.0f,
		D2D1_BITMAP_INTERPOLATION_MODE_LINEAR,
		RectF(20, 30, 38, 100));
}

void draw_light(ID2D1HwndRenderTarget* pRenderTarget, ID2D1Bitmap* lightBitmap, float cen_x, float cen_y) {
	pRenderTarget->DrawBitmap(
		lightBitmap,
		RectF(cen_x - 485, cen_y - 255, cen_x + 480, cen_y + 360),
		100.0f,
		D2D1_BITMAP_INTERPOLATION_MODE_LINEAR);
}