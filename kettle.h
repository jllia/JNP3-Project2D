class Kettle {

	float kettle_x = 300;
	float kettle_y = -35;

public:

	Kettle() {};

	std::pair<float, float> get_mug() {
		return std::make_pair(kettle_x, kettle_y);
	}

	void update_kettle(bool position, float value) {
		if (position) {
			kettle_x = value;
		}
		else {
			kettle_y = value;
		}
	}

	void draw_stable(float cen_x, float cen_y, ID2D1HwndRenderTarget* pRenderTarget) {
		pRenderTarget->DrawBitmap(
			kettleBitmap,
			D2D1::RectF(cen_x - kettle_x - 30, cen_y - kettle_y - 17, cen_x - kettle_x + 50, cen_y - kettle_y + 48),
			1.0f,
			D2D1_BITMAP_INTERPOLATION_MODE_LINEAR);
	}

	void draw_pouring(float cen_x, float cen_y, ID2D1HwndRenderTarget* pRenderTarget) {
		pRenderTarget->DrawBitmap(
			kettleBitmap,
			D2D1::RectF(cen_x - kettle_x - 30, cen_y - kettle_y - 85, cen_x - kettle_x + 50, cen_y - kettle_y - 20),
			1.0f,
			D2D1_BITMAP_INTERPOLATION_MODE_LINEAR);
	}

	void draw_moving(float cen_x, float cen_y, float mouse_x, ID2D1HwndRenderTarget* pRenderTarget) {
		float left = max(mouse_x, cen_x - 300) - 30;
		float right = min(left, cen_x + 240);

		pRenderTarget->DrawBitmap(
			kettleBitmap,
			D2D1::RectF(right, cen_y - kettle_y - 17, right + 80, cen_y - kettle_y + 48),
			1.0f,
			D2D1_BITMAP_INTERPOLATION_MODE_LINEAR);
	}

	ID2D1Bitmap* kettleBitmap = nullptr;
};