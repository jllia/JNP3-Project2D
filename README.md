# JNP3-Project2D

Projekt to pełnoekranowa scena interaktywna przedstawiająca kuchnię. Zawiera elementy zarówno ulegające zmianie pod wpływem czasu jak i ingerencji użytkownika. Miejsca podlegające edycji można odkryć poprzez zmiane koloru okrągłego kursora podczas najechania na nie - z szarego na czerwony. 

W projekcie zawarte są wszystkie wymagane elementy, liczne mapy bitowe, napisy, gradient, ścieżki oraz kształty geometryczne.
