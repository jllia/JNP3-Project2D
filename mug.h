class Mug {

	float mug_x = 215;
	float mug_y = 30;

	float tea_y = 71;
	float tea_x = 228;
	bool show_tea = false;

	bool show_mug = false;
	bool open_mug = false;

public:

	Mug() {};

	std::pair<float, float> get_mug() {
		return std::make_pair(mug_x, mug_y);
	}

	std::pair<float, float> get_tea() {
		return std::make_pair(tea_x, tea_y);
	}

	void update_tea(bool position, float value) {
		if (position) {
			tea_x = value;
		}
		else {
			tea_y = value;
		}
	}

	bool if_show_mug() {
		return show_mug;
	}

	bool if_show_tea() {
		return show_tea;
	}

	void update_show_tea(bool update) {
		show_tea = update;
	}

	void update_show_mug(bool update) {
		show_mug = update;
	}

	bool get_mug_open() {
		return open_mug;
	}

	void draw_stable(float cen_x, float cen_y, ID2D1HwndRenderTarget* pRenderTarget) {
		if ((mug_y == 30 && show_mug) || mug_y == -75) {
			pRenderTarget->DrawBitmap(
				mugBitmap,
				D2D1::RectF(cen_x - mug_x - 20, cen_y - mug_y - 15, cen_x - mug_x + 15, cen_y - mug_y + 15),
				1.0f,
				D2D1_BITMAP_INTERPOLATION_MODE_LINEAR);
		}
	}

	void draw_stable_tea(float cen_x, float cen_y, ID2D1HwndRenderTarget* pRenderTarget) {
		if ((tea_y == 71 && show_tea)) {
			pRenderTarget->DrawBitmap(
				teaBitmap,
				D2D1::RectF(cen_x - tea_x - 12, cen_y - tea_y + 5, cen_x - tea_x + 0, cen_y - tea_y + 15),
				1.0f,
				D2D1_BITMAP_INTERPOLATION_MODE_LINEAR,
				D2D1::RectF(0, 350, 170, 500));
		}
		else if (tea_y == -66) {
			pRenderTarget->DrawBitmap(
				teaBitmap,
				D2D1::RectF(cen_x - tea_x - 12, cen_y - tea_y - 10, cen_x - tea_x + 12, cen_y - tea_y + 15),
				1.0f,
				D2D1_BITMAP_INTERPOLATION_MODE_LINEAR);
		}
	}

	void draw_moving(float cen_x, float cen_y, float mouse_y, ID2D1HwndRenderTarget* pRenderTarget) {
		float up = max(mouse_y, cen_y - 10) - 30;
		float down = min(up, cen_y + 75);

		pRenderTarget->DrawBitmap(
			mugBitmap,
			D2D1::RectF(cen_x - mug_x - 20, down - 15, cen_x - mug_x + 15, down + 15),
			1.0f,
			D2D1_BITMAP_INTERPOLATION_MODE_LINEAR);
	}

	void stop_moving(float cen_x, float cen_y, float mouse_y, bool cupboard_open, ID2D1HwndRenderTarget* pRenderTarget) {

		if (abs(mouse_y - cen_y + 30) <= abs(mouse_y - (cen_y + 75)) && cupboard_open) {
			mug_y = 30;
			open_mug = false;
		}
		else {
			mug_y = -75;
			open_mug = true;
		}
	}

	void draw_moving_tea(float cen_x, float cen_y, float mouse_y, ID2D1HwndRenderTarget* pRenderTarget) {
		float up = max(mouse_y, cen_y - 25) - 50;
		float down = min(up, cen_y + 66);

		pRenderTarget->DrawBitmap(
			teaBitmap,
			D2D1::RectF(cen_x - tea_x - 12, down - 10, cen_x - tea_x + 12, down + 15),
			1.0f,
			D2D1_BITMAP_INTERPOLATION_MODE_LINEAR);
	}

	ID2D1Bitmap* mugBitmap = nullptr;
	ID2D1Bitmap* teaBitmap = nullptr;

};